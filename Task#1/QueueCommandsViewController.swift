//
//  QueueCommandsViewController.swift
//  Task#1
//
//  Created by Erinson Villarroel on 07/06/2022.
//


/* I think there are many ways to make a service but if this is what I'm understanding I think it can be something like this one
 
 this service simulates the calling of 5 queued tasks */


import UIKit

class QueueCommandsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        queueCommands()
    }
    
    private func queueCommands() {
        
        let globalQueue = DispatchQueue.global()

        func task(id number: Int) {
            print("task \(number) , current task \(Thread.current)")
            sleep(UInt32.random(in: 1...3))
            print("task \(number) completed")
        }
        print("------Start execute Serially------")
        for i in 1...5 {
            globalQueue.sync {
                task(id: i)
            }
        }
        print("------Start execute Concurrently------")
        for i in 1...5 {
            globalQueue.async {
                task(id: i)
            }
        }
    }
}


