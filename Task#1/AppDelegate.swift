//
//  AppDelegate.swift
//  Task#1
//
//  Created by Erinson Villarroel on 07/06/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = QueueCommandsViewController()
        window.makeKeyAndVisible()
        self.window = window
        return true
    }
}

